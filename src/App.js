import React from 'react';
import { Column, Row } from 'simple-flexbox';
import { StyleSheet, css } from 'aphrodite';
import SidebarComponent from './components/sidebar/SidebarComponent';
import HeaderComponent from './components/header/HeaderComponent';
import './App.css';
import CheckProduct from './components/CheckProduct';

const styles = StyleSheet.create({
    container: {
        height: '100%',
        minHeight: '100vh'
    },
    content: {
        marginTop: 54
    },
    mainBlock: {
        backgroundColor: '#F7F8FC',
        padding: 30
    }
});

class App extends React.Component {
    constructor(props) {
        super(props);
    }

    state = {
        selectedItem: 'Check product',
        contentPage: null,
        code: '',
        trackingCode: false
    };

    componentDidMount() {
        const search = this.props.location.search;
        if (search.length !== 0) {
            const result = JSON.parse('{"' + decodeURI(search.substring(1)).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g, '":"') + '"}');
            this.setState({
                code: result.code,
                contentPage: <CheckProduct code={result.code} handleTracking={() => this.setState({ trackingCode: true })}/>
            });
        }
        window.addEventListener('resize', this.resize);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.resize);
    }

    resize = () => this.forceUpdate();

    render() {
        const { selectedItem, contentPage, code, trackingCode } = this.state;

        return (
            <Row className={css(styles.container)}>
                <SidebarComponent selectedItem={selectedItem} contentPage={contentPage} code={code} trackingCode={trackingCode}
                    onChange={(selectedItem, contentPage) => this.setState({ selectedItem, contentPage })}
                    handleTracking={() => this.setState({ trackingCode: true })} />
                <Column flexGrow={1} className={css(styles.mainBlock)}>
                    <HeaderComponent title={selectedItem} />
                    <div className={css(styles.content)}>
                        {this.state.contentPage}
                    </div>
                </Column>
            </Row>
        );
    }
}

export default App;
