import axios from 'axios';
import React, { Component } from 'react';
import { Card, Empty, Timeline } from 'antd';

export default class extends Component {
    constructor(props) {
        super(props);
        this.state = {
            product: '',
            producer: '',
            image: '',
            result: null,
            medicines: [],
            root: '',
            startDay: '',
            endDay: ''
        }
    }
    componentDidMount() {
        axios.get('https://kdtrace.xyz/api/enduser/get-product-info?code=' + this.props.code)
            .then(response => {
                if (response.data.status === 200 && response !== null) {
                    this.setState({
                        product: response.data.result.productModel,
                        producer: response.data.result.producerModel,
                        image: response.data.result.productModel.image,
                        result: true
                    });
                } else {
                    this.setState({
                        result: false
                    });
                    console.log('problem');
                }
            })
            .catch(error => {
                this.setState({
                    result: false
                });
                console.log(error);
            });
        this.getRoot();
    }

    getRoot() {
        axios.get('https://kdtrace.xyz/api/enduser/get-root-product?code=' + this.props.code)
            .then(response => {
                if (response.data.status === 200 && response !== null) {
                    console.log({ response });
                    this.setState({
                        medicines: response.data.result.medicines,
                        root: response.data.result.root,
                        startDay: response.data.result.startDay,
                        endDay: response.data.result.endDay,
                    });
                } else {
                    console.log('problem');
                }
            })
            .catch(error => {
                console.log(error);
            });
    }

    render() {
        const { product, producer, result, image, medicines, root, startDay, endDay } = this.state;

        var imageString = "[raucu.jpg]"
        if (image !== "" && image !== undefined && image !== null && image !== "[]") { imageString = image; }
        imageString = imageString.slice(1, imageString.length - 1).split(",");
        var imageList = [];
        for (var i = 0; i < imageString.length; i++) {
            imageList.push(
                <div>
                    <hr />
                    <img src={imageString[i].trim()} style={{ maxWidth: "500px" }} width="100%" alt="image" />
                </div>
            );
        }
        if (result === null) {
            return (
                <Card
                    hoverable
                    style={{ width: '100%', maxWidth: '500px' }}
                    cover={<img alt="example" src="loading2.gif" />}
                >
                </Card>
            );
        } else if (!result) {
            return (
                <Empty />
            );
        } else {
            return (
                <div>
                    <Timeline mode='left'>
                        <Timeline.Item label={<h3>Name of product</h3>} color="red"><h3>{product.name}</h3></Timeline.Item>
                        <Timeline.Item label={<h3>Model</h3>} color="cyan"><h3>KDTRACE0000{product.id}</h3></Timeline.Item>
                        <Timeline.Item label={<h3>Type</h3>} color="pink"><h3>{product.type}</h3></Timeline.Item>
                        <Timeline.Item label={<h3>Root Type</h3>} color="pink"><h3>{root}</h3></Timeline.Item>
                        <Timeline.Item label={<h3>Start Day</h3>} color="pink"><h3>{startDay}</h3></Timeline.Item>
                        <Timeline.Item label={<h3>Harvest Day</h3>} color="pink"><h3>{endDay}</h3></Timeline.Item>
                        <Timeline.Item label={<h3>Medicine History</h3>} color="pink">
                        {
                            medicines.map(medicine => {
                                return <h3><div>{medicine.medicine} ({medicine.time})</div></h3>
                            })
                        }
                        </Timeline.Item>
                        <Timeline.Item label={<h3>Manufacturing Date</h3>} color="navy"><h3>{product.mfg}</h3></Timeline.Item>
                        <Timeline.Item label={<h3>Expiry Date</h3>} color="green"><h3>{product.exp}</h3></Timeline.Item>
                        {/* <Timeline.Item label={<h3>Producer</h3>}  ><h3>Company name: {producer.companyName}</h3></Timeline.Item>
                        <Timeline.Item ><h3>Email: {producer.email}</h3></Timeline.Item>
                        <Timeline.Item ><h3>Address: {producer.address}</h3></Timeline.Item>
                        <Timeline.Item ><h3>Phone: {producer.phone}</h3></Timeline.Item> */}
                    </Timeline>
                    <Card style={{ color: "maroon" }} type="inner" title="Image">
                        {imageList}
                    </Card>
                </div>
            );
        }
    }
}