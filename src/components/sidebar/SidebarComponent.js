import React from 'react';
import { Column, Row } from 'simple-flexbox';
import { StyleSheet, css } from 'aphrodite';
import LogoComponent from './LogoComponent';
import MenuItemComponent from './MenuItemComponent';
import IconOverview from '../../assets/icon-overview.js';
import IconTickets from '../../assets/icon-tickets.js';
import IconIdeas from '../../assets/icon-ideas.js';
import IconContacts from '../../assets/icon-contacts';
import IconArticles from '../../assets/icon-articles';
import IconSettings from '../../assets/icon-settings';
import IconBurger from '../../assets/icon-burger';
import ScanQRCode from '../ScanQRCode';
import CheckProduct from '../CheckProduct';
import Report from '../Report';
import ProductInfo from '../ProductInfo';

const styles = StyleSheet.create({
    burgerIcon: {
        cursor: 'pointer',
        position: 'absolute',
        left: 24,
        top: 34
    },
    container: {
        backgroundColor: '#004445',
        width: 255,
        paddingTop: 32,
        height: 'calc(100% - 32px)'
    },
    containerMobile: {
        transition: 'left 0.5s, right 0.5s',
        position: 'absolute',
        width: 255,
        height: 'calc(100% - 32px)',
        zIndex: 901
    },
    mainContainer: {
        height: '100%',
        minHeight: '100vh'
    },
    mainContainerMobile: {
        position: 'absolute',
        width: '100vw',
        minWidth: '100%',
        top: 0,
        left: 0
    },
    menuItemList: {
        marginTop: 52
    },
    outsideLayer: {
        position: 'absolute',
        width: '100vw',
        minWidth: '100%',
        height: '100%',
        backgroundColor: 'rgba(0,0,0,.50)',
        zIndex: 900
    },
    separator: {
        borderTop: '1px solid #DFE0EB',
        marginTop: 16,
        marginBottom: 16,
        opacity: 0.06
    },
    hide: {
        left: -255
    },
    show: {
        left: 0
    }
});

class SidebarComponent extends React.Component {
    state = { expanded: false };

    onItemClicked = (item, page) => {
        this.setState({ expanded: false });
        return this.props.onChange(item, page);
    }

    isMobile = () => window.innerWidth <= 768;

    toggleMenu = () => this.setState(prevState => ({ expanded: !prevState.expanded }));

    renderBurger = () => {
        return <div onClick={this.toggleMenu} className={css(styles.burgerIcon)}>
            <IconBurger />
        </div>
    }
    redirect = (address) => {
        window.open(address, "_blank");
    }

    render() {
        const { expanded } = this.state;
        const isMobile = this.isMobile();

        return (
            <div style={{ position: 'relative' }}>
                <Row className={css(styles.mainContainer)} breakpoints={{ 768: css(styles.mainContainerMobile) }}>
                    {(isMobile && !expanded) && this.renderBurger()}
                    <Column className={css(styles.container)} breakpoints={{ 768: css(styles.containerMobile, expanded ? styles.show : styles.hide) }}>
                        <LogoComponent />
                        <Column className={css(styles.menuItemList)}>
                            <MenuItemComponent
                                title="Product information" icon={IconIdeas}
                                onClick={() => this.onItemClicked('Product information', <ProductInfo code={this.props.code} />)}
                                active={this.props.selectedItem === 'Product information'} />
                            <MenuItemComponent
                                title="Check product" icon={IconOverview}
                                onClick={() => this.onItemClicked('Check product', <CheckProduct handleTracking={this.props.handleTracking} code={this.props.code} />)}
                                active={this.props.selectedItem === 'Check product'}
                            />
                            <MenuItemComponent
                                title="Scan QRCode" icon={IconTickets}
                                onClick={() => this.onItemClicked('Scan QRCode', <ScanQRCode />)}
                                active={this.props.selectedItem === 'Scan QRCode'}
                            />
                            {
                                this.props.trackingCode ?
                                    <MenuItemComponent
                                        title="Report" icon={IconArticles}
                                        onClick={() => this.onItemClicked('Report', <Report code={this.props.code} />)}
                                        active={this.props.selectedItem === 'Report'} />
                                    : null
                            }
                            <div className={css(styles.separator)}></div>
                            <MenuItemComponent
                                title="Blockchain System" icon={IconSettings}
                                onClick={() => this.redirect('http://explorer.kdtrace.xyz/#/')} />
                            <MenuItemComponent
                                title="About us" icon={IconContacts}
                                onClick={() => this.redirect('/about-us')} />
                        </Column>
                    </Column>
                    {isMobile && expanded && <div className={css(styles.outsideLayer)} onClick={this.toggleMenu}></div>}
                </Row>
            </div>
        );
    };
}

export default SidebarComponent;
