import React from 'react';
import { Row } from 'simple-flexbox';
import { StyleSheet, css } from 'aphrodite';

const styles = StyleSheet.create({
    container: {
        marginLeft: 32,
        marginRight: 32
    },
    avatar: {
        height: 35,
        width: 35,
        borderRadius: 50,
        marginLeft: 14,
        border: '1px solid #DFE0EB',
    },
    cursorPointer: {
        cursor: 'pointer'
    },
    title: {
        fontFamily: 'Muli',
        fontStyle: 'normal',
        fontWeight: 'bold',
        fontSize: 19,
        lineHeight: '24px',
        letterSpacing: '1px',
        color: '#dde2ff',
        opacity: 0.7,
        marginLeft: 6
    }
});

function LogoComponent() {
    return (
        <Row className={css(styles.container)} horizontal="center" vertical="center">
            <img src="logo-80x80.png" alt="avatar" className={css(styles.avatar, styles.cursorPointer)} />
            <span className={css(styles.title)}>KDTrace</span>
        </Row>
    );
}

export default LogoComponent;
