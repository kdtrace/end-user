import React, { Component } from "react";
import QrReader from 'react-qr-reader'
import { Button, Card } from "antd";
import "antd/dist/antd.css";
import { Link } from 'react-router-dom'

export default class ScanQRCode extends Component {
  constructor(props) {
    super(props);
    this.state = {
      delay: 300,
      result: "No result",
      contentButton: "Scanning",
      loadingButton: true
    };
    this.handleScan = this.handleScan.bind(this);
    this.videoStream = React.createRef()
  }
  handleScan(data) {
    if (data) {
      this.setState({
        result: data,
        contentButton: "Check",
        loadingButton: false
      });

    }
  }
  handleError(err) {
    console.error(err);
  }
  handleClick = e => {
    var n = this.state.result.lastIndexOf("/");
    var link = this.state.result.substring(n + 1);
    window.open(link);
    window.location.reload();
  };
  render() {
    return (
      <div>
        <Card title="Scan QRCode"
          style={{ maxWidth: '500px' }}>
          <QrReader
            delay={500}
            facingMode={'environment'}
            onError={this.handleError}
            onScan={this.handleScan}
            style={{ width: '100%' }}
            ref={this.videoStream}
          />
        </Card>
        <Link to="this.state.result"><h3>{this.state.result}</h3></Link>
        <Button type="primary" loading={this.state.loadingButton} onClick={() => this.handleClick()}>
          {this.state.contentButton}
        </Button>
      </div>
    );
  }
}

