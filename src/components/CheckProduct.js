import axios from 'axios';
import React, { Component } from 'react';
import TimeLineProcess from './TimeLineProcess';
import { Popover, Button, Input, Modal } from 'antd';

const { Search } = Input;

export default class extends Component {
    constructor(props) {
        super(props);
        this.state = {
            trackingCode: '',
            trankingCount: ''
        }
    }
    componentDidMount() { }

    handleTrackingCode = (trackingCode) => {
        axios.patch('https://kdtrace.xyz/api/enduser/tracking-code?code=' + this.props.code + '&otp=' + trackingCode)
            .then(response => {
                if (response.data.status === 200 && response !== null && response.data.result !== null) {
                    this.setState({
                        trankingCount: response.data.result
                    });
                    if (response.data.result === 1) {
                        this.trackingSuccess()
                    } else {
                        this.trackingWarning()
                    }
                    this.props.handleTracking();
                } else {
                    this.trackingError()
                }
            })
            .catch(error => {
                console.log(error);
                this.trackingError()
            });
    };
    trackingSuccess() {
        Modal.success({
            content: 'The product was verified. Set your mind at rest!',
        });
    }
    trackingWarning() {
        Modal.warning({
            title: 'Warning',
            content: 'This code was verified ' + this.state.trankingCount + ' times. The product may be fake, we need your report in order to improve our goods.'
        });
    }
    trackingError() {
        Modal.error({
            title: 'Sorry',
            content: 'Something is wrong!',
        });
    }
    render() {
        return (
            <div>
                <Popover placement="bottomLeft" title={"Tracking code"} content={"This is a secret code under the scratch card field. You can verify your product to meet the quality standards."} trigger="click">
                    <Button type="ghost" style={{ width: '58%', maxWidth: '270px' }}><b>Check your QRCode: </b></Button>
                </Popover>
                <Search style={{ width: '42%', maxWidth: '230px' }} placeholder="..."
                    onSearch={value => this.handleTrackingCode(value)} enterButton />
                <TimeLineProcess code={this.props.code} />
            </div>
        );
    }
}