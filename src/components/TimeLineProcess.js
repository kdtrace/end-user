import { Timeline, TimelineItem } from 'vertical-timeline-component-for-react';
import axios from 'axios';
import React, { Component } from 'react';
import { Empty, Tag, Card, Rate } from 'antd';
import { Link } from 'react-router-dom';
import {
    Fingerprint, Assignment, EmailRounded, LocationCity, PhoneCallback, Code, Web, ConfirmationNumber, ExceptionOutlined,
    EmojiTransportation, AccountTree, Fastfood, AddComment, LocalShipping, FormatListNumbered, PermIdentity
} from '@material-ui/icons';
import Clock from 'react-live-clock';
import 'antd/dist/antd.css';
import ExploreOffIcon from '@material-ui/icons/ExploreOff';


export default class extends Component {
    constructor(props) {
        super(props);
        this.state = {
            product: [],
            producer: [],
            delivery_time: [],
            transport: [],
            deliveryTruck: [],
            receipt_time: [],
            distributor: [],
            image: '',
            result: null,
            rate_producer: 5,
            rate_transport: 5,
            rate_distributor: 5
        }
    }


    componentDidMount() {
        axios.get('https://kdtrace.xyz/api/enduser/get-product-info?code=' + this.props.code)
            .then(response => {
                if (response.data.status === 200 && response !== null) {
                    this.setState({
                        product: response.data.result.productModel,
                        producer: response.data.result.producerModel,
                        delivery_time: response.data.result.delivery_at,
                        transport: response.data.result.transportModel,
                        deliveryTruck: response.data.result.deliveryTruckModel,
                        receipt_time: response.data.result.receipt_at,
                        distributor: response.data.result.distributorModel,
                        image: response.data.result.productModel.image,
                        result: true
                    });
                    this.getRate(response.data.result.producerModel.id, 'ROLE_PRODUCER');
                    this.getRate(response.data.result.transportModel.id, 'ROLE_TRANSPORT');
                    this.getRate(response.data.result.distributorModel.id, 'ROLE_DISTRIBUTOR');
                } else {
                    this.setState({
                        result: false
                    });
                    console.log('problem');
                }
            })
            .catch(error => {
                this.setState({
                    result: false
                });
                console.log(error);
            });
    }

    getRate(id, role) {
        axios.get('https://kdtrace.xyz/api/enduser/getRate?id=' + id + '&role=' + role)
            .then(response => {
                if (response.data.status === 200 && response !== null) {
                    switch (role) {
                        case 'ROLE_PRODUCER':
                            this.setState({
                                rate_producer: response.data.result,
                            });
                            break;
                        case 'ROLE_TRANSPORT':
                            this.setState({
                                rate_transport: response.data.result,
                            });
                            break;
                        case 'ROLE_DISTRIBUTOR':
                            this.setState({
                                rate_distributor: response.data.result,
                            });
                            break;
                    }
                } else {
                    console.log('problem');
                }
            })
            .catch(error => {
                console.log(error);
            });
    }

    render() {
        const { product, producer, delivery_time, transport, deliveryTruck, receipt_time, distributor, result, image } = this.state;

        var imageString = "[raucu.jpg]"
        if (image !== "" && image !== undefined && image !== null && image !== "[]") { imageString = image; }
        imageString = imageString.slice(1, imageString.length - 1).split(",");
        var imageList = [];
        for (var i = 0; i < imageString.length; i++) {
            imageList.push(
                <div>
                    <hr />
                    <img src={imageString[i].trim()} width="100%" alt="image" />
                </div>
            );
        }
        if (result === null) {
            return (
                <Card
                    hoverable
                    style={{ width: '100%', maxWidth: '500px' }}
                    cover={<img alt="example" src="loading.gif" />}
                >
                </Card>
            );
        } else if (!result) {
            return (
                <Empty />
            );
        } else {
            const moment = require('moment');
            const today = moment();
            var expDate = moment(product.exp, "YYYY-MM-DD");
            var subDay = expDate.diff(today, 'days') + 1;
            var exp = true;
            if (subDay > 0) {
                exp = false;
            }
            function roundHalf(num) {
                return Math.round(num*2)/2;
            }
            return (
                <div align="center" style={{ backgroundImage: `url('fruit_and_vegetables6.jpg')` }}>
                    <Timeline lineColor={'#004445'} >
                        <TimelineItem
                            key="001"
                            dateText={product.mfg.replace('-', '/').replace('-', '/')}
                            dateInnerStyle={{ background: '#e6d72a', color: '#000' }}
                            bodyContainerStyle={{
                                background: 'honeydew',
                                padding: '20px',
                                borderRadius: '8px',
                                boxShadow: '0.5rem 0.5rem 2rem 0 rgba(0, 0, 0, 0.2)',
                            }}
                        >
                            <h3 align="right"><Tag color="blue">Manufacturer Information</Tag></h3>
                            <div style={{ display: "flex", justifyContent: "space-between" }} >
                                <h3 ><Assignment /><b> <span />PRODUCER</b></h3>
                                <h3> <Rate allowHalf value={roundHalf(this.state.rate_producer)} /></h3>
                            </div>
                            <h4>-----</h4>
                            <h4><Fingerprint /> <sup style={{ fontSize: "100%" }}><b>Name:</b> {producer.companyName}</sup></h4>
                            <h4><Code /> <sup style={{ fontSize: "100%" }}><b>Code:</b> PRODUCER-0000{producer.id}</sup></h4>
                            <h4><Web /> <sup style={{ fontSize: "100%" }}><b>Website:</b> <Link onClick={(event) => { event.preventDefault(); window.open(producer.website) }}>{producer.website}</Link></sup></h4>
                            <h4><ConfirmationNumber /> <sup style={{ fontSize: "100%" }}><b>Tax identification number:</b> {producer.tin}</sup></h4>
                            <h4><EmailRounded /> <sup style={{ fontSize: "100%" }}><b>Email:</b> {producer.email}</sup></h4>
                            <h4><LocationCity /> <sup style={{ fontSize: "100%" }}><b>Address:</b> {producer.address}</sup></h4>
                            <h4><PhoneCallback /> <sup style={{ fontSize: "100%" }}><b>Phone:</b> {producer.phone}</sup></h4>
                        </TimelineItem>
                        <TimelineItem
                            key="002"
                            dateText={delivery_time}
                            dateInnerStyle={{ background: '#61b8ff', color: '#000' }}
                            bodyContainerStyle={{
                                background: 'aliceblue',
                                padding: '20px',
                                borderRadius: '8px',
                                boxShadow: '0.5rem 0.5rem 2rem 0 rgba(0, 0, 0, 0.2)',
                            }}
                        >
                            <h3 align="right"><Tag color="blue">Transporter Information</Tag></h3>
                            <div style={{ display: "flex", justifyContent: "space-between" }} >
                                <h3 ><EmojiTransportation /><b>TRANSPORTER</b></h3>
                                <h3> <Rate allowHalf value={roundHalf(this.state.rate_transport)} /></h3>
                            </div>
                            <h4>-----</h4>
                            <h4><Fingerprint /> <sup style={{ fontSize: "100%" }}><b>Name:</b> {transport.companyName}</sup></h4>
                            <h4><Code /> <sup style={{ fontSize: "100%" }}><b>Code:</b> TRANSPORTER-0000{transport.id}</sup></h4>
                            <h4><Web /> <sup style={{ fontSize: "100%" }}><b>Website:</b> <Link onClick={(event) => { event.preventDefault(); window.open(transport.website) }}>{transport.website}</Link></sup></h4>
                            <h4><ConfirmationNumber /> <sup style={{ fontSize: "100%" }}><b>Tax identification number:</b> {transport.tin}</sup></h4>
                            <h4><EmailRounded /> <sup style={{ fontSize: "100%" }}><b>Email:</b> {transport.email}</sup></h4>
                            <h4><LocationCity /> <sup style={{ fontSize: "100%" }}><b>Address:</b> {transport.address}</sup></h4>
                            <h4><PhoneCallback /> <sup style={{ fontSize: "100%" }}><b>Phone:</b> {transport.phone}</sup></h4>
                            <hr />
                            <h3 ><LocalShipping />
                                <b> <span />DELIVERY TRUCK</b></h3>
                            <h4>-----</h4>
                            <h4><PermIdentity /> <sup style={{ fontSize: "100%" }}><b>Automaker:</b> {deliveryTruck.autoMaker}</sup></h4>
                            <h4><FormatListNumbered /> <sup style={{ fontSize: "100%" }}><b>Number plate:</b> {deliveryTruck.numberPlate}</sup></h4>
                        </TimelineItem>
                        <TimelineItem
                            key="003"
                            dateText={receipt_time}
                            dateInnerStyle={{ background: '#76bb7f', color: '#000' }}
                            bodyContainerStyle={{
                                background: 'beige',
                                padding: '20px',
                                borderRadius: '8px',
                                boxShadow: '0.5rem 0.5rem 2rem 0 rgba(0, 0, 0, 0.2)',
                            }}
                        >
                            <h3 align="right"><Tag color="blue">Distributor Information</Tag></h3>
                            <div style={{ display: "flex", justifyContent: "space-between" }} >
                                <h3 ><AccountTree /><b>DISTRIBUTOR</b></h3>
                                <h3> <Rate allowHalf value={roundHalf(this.state.rate_distributor)} /></h3>
                            </div>
                            <h4>-----</h4>
                            <h4><Fingerprint /> <sup style={{ fontSize: "100%" }}><b>Name:</b> {distributor.companyName}</sup></h4>
                            <h4><Code /> <sup style={{ fontSize: "100%" }}><b>Code:</b> DISTRIBUTOR-0000{distributor.id}</sup></h4>
                            <h4><Web /> <sup style={{ fontSize: "100%" }}><b>Website:</b> <Link onClick={(event) => { event.preventDefault(); window.open(distributor.website) }}>{distributor.website}</Link></sup></h4>
                            <h4><ConfirmationNumber /> <sup style={{ fontSize: "100%" }}><b>Tax identification number:</b> {distributor.tin}</sup></h4>
                            <h4><EmailRounded /> <sup style={{ fontSize: "100%" }}><b>Email:</b> {distributor.email}</sup></h4>
                            <h4><LocationCity /> <sup style={{ fontSize: "100%" }}><b>Address:</b> {distributor.address}</sup></h4>
                            <h4><PhoneCallback /> <sup style={{ fontSize: "100%" }}><b>Phone:</b> {distributor.phone}</sup></h4>
                        </TimelineItem>
                        <TimelineItem
                            key="004"
                            dateText={<div>{today.format('YYYY/MM/DD')} <Clock format={'HH:mm:ss'} ticking={true} timezone={'Asia/Ho_Chi_Minh'} /></div>}
                            dateInnerStyle={{ background: '#faaf08', color: '#000' }} bodyContainerStyle={{
                                background: 'snow',
                                padding: '20px',
                                borderRadius: '8px',
                                boxShadow: '0.5rem 0.5rem 2rem 0 rgba(0, 0, 0, 0.2)',
                            }}
                        >
                            <h3 style={{
                                display: "flex",// grid
                                justifyContent: "space-between",
                                paddingBottom: "7px"
                            }}>
                                <h3 align="right"><Tag color="blue">Now</Tag></h3>
                                <h3 align="right"><Tag color="blue">Product Information</Tag></h3>
                            </h3>
                            <h3 ><Fastfood />
                                <b> <span />PRODUCT</b></h3>
                            <h4>-----</h4>
                            {
                                exp ?
                                    <h3 style={{ color: 'red' }}><ExploreOffIcon /> <sup style={{ fontSize: "100%" }}>"<b>{product.name}</b>" is expired.</sup></h3>
                                    :
                                    <h4><AddComment /> <sup style={{ fontSize: "100%" }}>"<b>{product.name}</b>" will be expired in {subDay} day(s).</sup></h4>
                            }
                            {imageList}
                        </TimelineItem>
                    </Timeline>
                </div>
            );
        }
    }
}