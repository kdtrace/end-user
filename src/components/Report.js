import axios from 'axios';
import React, { Component } from 'react';
import { Button, Input, Modal } from 'antd';
import { Form, Card, Rate } from 'antd';

const layout = {
    labelCol: {
        span: 8,
    },
    wrapperCol: {
        span: 16,
    },
};

export default class extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: 'Anonymous',
            phone: '-',
            reportContent: ''
        }
    }
    handleSentFeedback = (values) => {
        if (this.state.report !== "") {
            axios.post('https://kdtrace.xyz/api/enduser/report', {
                code: this.props.code,
                name: values.Name,
                phone: values.Phone,
                reportContent: values.Report,
                rate: values.Rate
            })
                .then(response => {
                    if (response.data.status === 200 && response != null && response.data.result != null) {
                        Modal.info({
                            title: 'Report',
                            content: response.data.result,
                        });
                    }
                })
                .catch(error => {
                    Modal.error({
                        title: 'Error',
                        content: "Something is wrong! Can't submit your report",
                    });
                });
        }
        else {
            Modal.warning({
                title: 'Warning',
                content: 'Your report is empty, try again!'
            });
        }
    }

    onFinish = (values) => {
        console.log({ values });
        this.handleSentFeedback(values);
    };

    componentDidMount() { }
    render() {
        return (
            <Card title="Write your report"
                style={{ maxWidth: '700px' }}>
                <Form {...layout} onFinish={this.onFinish}>
                    <Form.Item
                        name={['Name']}
                        label="Name"
                    >
                        <Input placeholder="Anonymous" />
                    </Form.Item>
                    <Form.Item
                        name={['Phone']}
                        label="Phone number"
                    >
                        <Input placeholder="-" />
                    </Form.Item>
                    <Form.Item
                        name={['Report']}
                        label="Report"
                        rules={[
                            {
                                required: true
                            },
                        ]}
                    >
                        <Input.TextArea />
                    </Form.Item>
                    <Form.Item
                        name={['Rate']}
                        label="Rate"
                        rules={[
                            {
                                required: true
                            },
                        ]}
                    >
                        <Rate />
                    </Form.Item>
                    <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
                        <Button type="primary" htmlType="submit">
                            Submit
                        </Button>
                    </Form.Item>
                </Form>
            </Card>
        );
    }
}